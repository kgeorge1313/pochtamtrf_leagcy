<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE></TITLE>
	<META NAME="GENERATOR" CONTENT="OpenOffice 4.1.3  (Win32)">
	<META NAME="CREATED" CONTENT="20190809;14163026">
	<META NAME="CHANGED" CONTENT="20190812;9052126">
	<STYLE TYPE="text/css">
	<!--
		@page { size: 21cm 29.7cm; margin: 2cm }
		P { margin-bottom: 0.21cm }
		STRONG { font-weight: bold }
		A:link { color: #000080; so-language: zxx; text-decoration: none }
	-->
	</STYLE>
</HEAD>
<BODY LANG="ru-RU" LINK="#000080" VLINK="#800000" DIR="LTR" style="padding: 10px;" >
<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><STRONG><FONT COLOR="#000000"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5><SPAN STYLE="font-style: normal"><B>ПОЧТАМТЪ</B></SPAN></FONT></FONT></FONT></STRONG><FONT COLOR="#000000"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">&nbsp;–
одна из ведущих Российских компаний в
области оказания почтовых и логистических
услуг. </SPAN></SPAN></FONT></FONT></FONT>
</P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Мы
осуществляем доставку корреспонденции,
а также грузов до 5 килограммов по всей
России.</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; widows: 2; orphans: 2"><FONT COLOR="#000000"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5 STYLE="font-size: 20pt"><B><SPAN LANG="ru-RU">Курьерская
</SPAN>доставка:</B></FONT></FONT></FONT></P>
<TABLE WIDTH=963 BORDER=0 CELLPADDING=6 CELLSPACING=0>
	<COL WIDTH=704>
	<COL WIDTH=236>
	<TR VALIGN=TOP>
		<TD WIDTH=704>
			<OL>
				<LI><P ALIGN=LEFT><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Экспресс-вызов
				курьера</FONT></FONT></P>
				<LI><P ALIGN=LEFT><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Регуларная
				отправка и получение корреспонденци
				абонентов по расписанию</FONT></FONT></P>
				<LI><P ALIGN=LEFT><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Доставка
				курьером</FONT></FONT></P>
				<LI><P ALIGN=LEFT><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Трекинг
				корреспонденции</FONT></FONT></P>
				<LI><P ALIGN=LEFT><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Уведомление
				в реальном времени</FONT></FONT></P>
			</OL>
		</TD>
		<TD WIDTH=236>
			<P ALIGN=JUSTIFY><IMG SRC="page1_html_4d0fb957.png" NAME="Графический объект6" ALIGN=LEFT WIDTH=233 HEIGHT=197 BORDER=0><BR CLEAR=LEFT><BR>
			</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="font-style: normal; widows: 2; orphans: 2"><BR><BR>
</P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; widows: 2; orphans: 2"><FONT COLOR="#000000"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5 STYLE="font-size: 20pt"><B>Мы
доставляем:</B></FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Деловую
корреспонденцию (включая первичную
документацию, бухгалтерскую отчетность
и налоговые декларации, юридическую
документацию)</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Каталоги</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Буклеты</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Пригласительные
письма</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Письма
и открытки</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Посылки
и бандероли</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Подарки</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Образцы
товаров (в том числе по каталогам
интернет-магазинов);</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000">– <FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Прочие
материалы (корме запрещенных к перевозке
в соответствии с статьей 22 Федерального
закона от 17.07.1999 г. № 176-ФЗ «О почтовой
связи»).</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<BR><BR>
</P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<FONT COLOR="#000000"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>Работая
с Компанией ПОЧТАМТЪ, Вы прежде всего
работаете с профессионалами, чей девиз
звучит, как: КЛИЕНТ ВСЕГДА ПРАВ</FONT></FONT></FONT></P>
<P ALIGN=JUSTIFY STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<BR><BR>
</P>
<TABLE WIDTH=963 BORDER=0 CELLPADDING=6 CELLSPACING=0>
	<COL WIDTH=309>
	<COL WIDTH=309>
	<COL WIDTH=309>
	<TR VALIGN=TOP>
		<TD WIDTH=309>
			<P><IMG SRC="page1_html_m2f5d9e05.png" NAME="Графический объект1" ALIGN=LEFT WIDTH=310 HEIGHT=310 BORDER=0><BR CLEAR=LEFT>
			<BR>
			</P>
		</TD>
		<TD WIDTH=309>
			<P><IMG SRC="page1_html_5adf670e.png" NAME="Графический объект2" ALIGN=LEFT WIDTH=310 HEIGHT=310 BORDER=0><BR CLEAR=LEFT><BR>
			</P>
		</TD>
		<TD WIDTH=309>
			<P><IMG SRC="page1_html_m746044e6.png" NAME="Графический объект3" ALIGN=LEFT WIDTH=310 HEIGHT=310 BORDER=0><BR CLEAR=LEFT><BR>
			</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="font-style: normal; font-weight: normal; widows: 2; orphans: 2">
<BR><BR>
</P>
<P ALIGN=LEFT STYLE="widows: 2; orphans: 2"><STRONG><FONT COLOR="#000000"><SPAN STYLE="text-decoration: none"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5 STYLE="font-size: 20pt"><SPAN STYLE="font-style: normal"><B><SPAN STYLE="background: #ffffff">Партнеры</SPAN></B></SPAN></FONT></FONT></SPAN></FONT></STRONG><STRONG><FONT COLOR="#000000"><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal"><SPAN STYLE="background: #ffffff">
</SPAN></SPAN></SPAN></FONT></FONT></FONT></STRONG>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0cm"><BR>
</P>
<TABLE WIDTH=100% BORDER=0 CELLPADDING=6 CELLSPACING=0>
	<COL WIDTH=64*>
	<COL WIDTH=64*>
	<COL WIDTH=64*>
	<COL WIDTH=64*>
	<TR VALIGN=TOP>
		<TD WIDTH=25%>
			<P ALIGN=CENTER><A HREF="http://r-aero.com/"><FONT COLOR="#0066cc"><FONT SIZE=4><B>Робоаэронавтика</B></FONT></FONT></A></P>
			<P ALIGN=CENTER><FONT COLOR="#0066cc"><FONT SIZE=4><B>Цифровая
			экономика</B></FONT></FONT></P>
		</TD>
		<TD WIDTH=25%>
			<P ALIGN=CENTER><BR>
			</P>
			<P ALIGN=CENTER>  <STRONG><A HREF="http://cps.group/"><FONT COLOR="#0066cc"><FONT FACE="Times New Roman, serif"><FONT SIZE=4 STYLE="font-size: 16pt"><SPAN STYLE="font-style: normal">cps.group</SPAN></FONT></FONT></FONT></A></STRONG></P>
			<P ALIGN=CENTER><BR>
			</P>
		</TD>
		<TD WIDTH=25%>
			<P LANG="en-US" ALIGN=CENTER><IMG SRC="page1_html_2457961a.png" NAME="Графический объект4" ALIGN=LEFT WIDTH=50 HEIGHT=50 BORDER=0><BR CLEAR=LEFT><A HREF="http://apteka.ru/sankt-peterburg/"><FONT COLOR="#0066cc"><FONT SIZE=4><B>Аптека.ru</B></FONT></FONT></A></P>
		</TD>
		<TD WIDTH=25%>
			<P ALIGN=CENTER><IMG SRC="page1_html_6f51c19e.png" NAME="Графический объект5" ALIGN=LEFT WIDTH=79 HEIGHT=68 BORDER=0><BR CLEAR=LEFT><A HREF="http://xn--d1aciseijom.xn--p1ai/"><FONT COLOR="#0066cc"><FONT SIZE=4><B>ФОНД
			&quot;ЩИТ&quot;</B></FONT></FONT></A></P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=CENTER STYLE="margin-bottom: 0cm"><BR>
</P>
<P STYLE="margin-bottom: 0cm"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0cm"><FONT COLOR="#000000"><STRONG><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5><SPAN STYLE="font-style: normal"><B>ПОЧТАМТЪ.рф</B></SPAN></FONT></FONT></STRONG><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>&nbsp;</FONT></FONT><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">©
Copyright 2019</SPAN></SPAN></FONT></FONT><FONT FACE="Segoe Script, sans-serif"><FONT SIZE=5>
</FONT></FONT></FONT>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0cm"><BR>
</P>
</BODY>
</HTML>