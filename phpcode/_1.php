<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE></TITLE>
	<META NAME="GENERATOR" CONTENT="OpenOffice 4.1.3  (Win32)">
	<META NAME="CREATED" CONTENT="20190814;17484563">
	<META NAME="CHANGED" CONTENT="20190819;3473249">
	<META HTTP-EQUIV="CACHE-CONTROL" content="NO-CACHE">
	<meta http-equiv="Cache-Control" content="no-store">
	<STYLE TYPE="text/css">
	</STYLE>
</HEAD>
<BODY LANG="ru-RU" DIR="LTR">
<TABLE WIDTH=963 BORDER=1 BORDERCOLOR="#000000" CELLPADDING=1 CELLSPACING=0 STYLE="page-break-before: always">
	<COL WIDTH=535>
	<COL WIDTH=422>
	<TR VALIGN=TOP>
		<TD ROWSPAN=2 WIDTH=535 HEIGHT=35>
			<P ALIGN=CENTER STYLE="font-weight: normal">
<FONT FACE="Times New Roman, serif">
<FONT SIZE=5 STYLE="font-size: 20pt">
<img width=60% height=95% src="form_title.png"></FONT></FONT></P>
<P  style="margin-bottom: 0.0cm;"ALIGN=CENTER>
<FONT SIZE=3>ООО «ПОЧТАМТЪ»<SPAN LANG="en-US">|
</SPAN><SPAN LANG="ru-RU">Санкт-Петербург</SPAN></FONT></P>
<P style="margin-bottom: 0.0cm;" ALIGN=CENTER><FONT SIZE=3><SPAN LANG="ru-RU">ИНН 7816281447
			</SPAN><SPAN LANG="en-US">| </SPAN>№ <SPAN LANG="ru-RU">лицензии
			158358</SPAN></FONT></P>
<P  style="margin-bottom: 0.0cm;" ALIGN=CENTER><FONT SIZE=3>почтамтъ.рф</FONT></P>
		</TD>
		<TD ROWSPAN=2 WIDTH=422>
			<P ALIGN=CENTER><FONT SIZE=4>Накладная</FONT></P>
<br>
			<P ALIGN=LEFT> <font size=6>№ %oid%</font>
			</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0cm; line-height: 0.25cm"><BR>
</P>
<TABLE WIDTH=963 BORDER=1 BORDERCOLOR="#000000" CELLPADDING=1 CELLSPACING=0>
	<COL WIDTH=292>
	<COL WIDTH=1>
	<COL WIDTH=331>
	<COL WIDTH=330>
	<TR>
		<TD COLSPAN=4 WIDTH=959 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=4>ОТПРАВИТЕЛЬ (<SPAN LANG="en-US">the
			sender): %sender%</SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=295 HEIGHT=18>
			<P ALIGN=CENTER><FONT SIZE=3><SPAN LANG="ru-RU">Город(</SPAN><SPAN LANG="en-US">city,town): %city%</SPAN>
			</FONT>
			</P>
		</TD>
		<TD WIDTH=331 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=3><SPAN LANG="ru-RU">Страна</SPAN>
			<SPAN LANG="en-US">(country): %country%</SPAN> </FONT>
			</P>
		</TD>
		<TD WIDTH=330 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=3>Индекс <SPAN LANG="en-US">(zip
			code): %zip%</SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=959 HEIGHT=18>
			<P ALIGN=CENTER><FONT SIZE=3><SPAN LANG="ru-RU">Адрес</SPAN>
			(<SPAN LANG="en-US">address): %address%</SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=959 HEIGHT=18 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=2>ОПИСАНИЕ ВЛОЖЕНИЯ
			<SPAN LANG="en-US">(description): %description%</SPAN></FONT></P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=292>
			<P ALIGN=CENTER><FONT SIZE=2><FONT FACE="Times New Roman, serif"></FONT>%tarif0%
			<FONT FACE="Times New Roman, serif">«Эконом»/</FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">Economy</SPAN></FONT></FONT></P>
		</TD>
		<TD COLSPAN=3 WIDTH=665>
			<P ALIGN=CENTER><FONT SIZE=2><FONT FACE="Times New Roman, serif"></FONT>%tarif2%
			<FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">Доставка
			после 18:</SPAN></FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">00
			(delivery after 6 p.m.)</SPAN></FONT></FONT></P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=292>
			<P ALIGN=CENTER><FONT SIZE=2><FONT FACE="Times New Roman, serif"></FONT>%tarif1%
			<FONT FACE="Times New Roman, serif">«Экспресс»/</FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">Express</SPAN></FONT></FONT></P>
		</TD>
		<TD COLSPAN=3 WIDTH=665>
			<P ALIGN=CENTER><FONT SIZE=2><FONT FACE="Times New Roman, serif"></FONT>%tarif3%
			<FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">Доставка
			по выходным</SPAN></FONT> <FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">(weekend
			delivery)</SPAN></FONT></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=959>
			<P STYLE="margin-bottom: 0.2cm"><BR><BR>
			</P>
			<P ALIGN=CENTER STYLE="margin-bottom: 0.0cm"><FONT SIZE=4 STYLE="font-size: 16pt">ОСОБЫЕ
			УСЛОВИЯ ИЛИ КОММЕНТАРИИ ПО ДОСТАВКЕ</FONT></P>
			<P ALIGN=CENTER STYLE="margin-bottom: 0.0cm"><FONT SIZE=4 STYLE="font-size: 16pt">(<SPAN LANG="en-US">special
			conditions or comments on delivery): %comments%</SPAN></FONT></P>
			<P><BR>
			</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=959 VALIGN=TOP>
			<P STYLE="margin-bottom: 0.2cm"><FONT SIZE=2><FONT SIZE=1 STYLE="font-size: 8pt"><B>Внимание!</B></FONT>
			<FONT SIZE=1 STYLE="font-size: 8pt">Графы «Город»,
			«Адрес», «ОТПРАВИТЕЛЬ», «Описание
			включения» заполняются ОТПРАВИТЕЛЕМ.
			Если данные графы заполнены некорректно
			или неполностью, курьерская служба
			ответственности за несвоевременную
			доставку не несет.</FONT></FONT></P>
			<P><FONT SIZE=1 STYLE="font-size: 8pt"><B>Attention!</B> Counts
			“City”, “Address”, “SENDER”, “<SPAN LANG="en-US">Description</SPAN>
			of inclusion” <SPAN LANG="en-US">are f</SPAN>ill<SPAN LANG="en-US">ed</SPAN>
			out by the SENDER. If these columns are filled incorrectly or
			incompletely, courier service is not <SPAN LANG="en-US">responsible</SPAN>
			for <SPAN LANG="en-US">late</SPAN> delivery.</FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=3 WIDTH=627 VALIGN=TOP>
			<P STYLE="margin-bottom: 0.2cm"><FONT SIZE=1 STYLE="font-size: 8pt"><B>Отправитель</B>
			подтверждает, чт ознакомлен с условиями
			доставки, а отправление не содержит
			предметы, запрещенные к пересылке.</FONT></P>
			<P><FONT SIZE=1 STYLE="font-size: 8pt"><B>The sender</B> confirms
			that <SPAN LANG="en-US">have</SPAN> <SPAN LANG="en-US">read </SPAN>the
			terms of delivery, and the <SPAN LANG="en-US">package</SPAN> does
			not contain prohibited <SPAN LANG="en-US">items or</SPAN>
			shipment.</FONT></P>
		</TD>
		<TD WIDTH=330>
			<P ALIGN=LEFT><FONT SIZE=3>Подпись (<SPAN LANG="en-US">signature):                             </SPAN></FONT></P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0cm; line-height: 0.02cm"><BR>
</P>
<TABLE WIDTH=962 BORDER=1 BORDERCOLOR="#000000" CELLPADDING=1 CELLSPACING=0>
	<COL WIDTH=534>
	<COL WIDTH=422>
	<TR VALIGN=TOP>
		<TD WIDTH=534>
			<P ALIGN=CENTER STYLE="margin-bottom: 0.2cm"><FONT SIZE=2>Фамилия
			курьера</FONT></P>
			<P ALIGN=CENTER><FONT SIZE=2>(<SPAN LANG="en-US">the name of the
			courier): %kfio% </SPAN></FONT></P>
		</TD>
		<TD WIDTH=422>
			<P ALIGN=CENTER STYLE="margin-bottom: 0.2cm"><FONT SIZE=2>Дата
			приема</FONT></P>
			<P ALIGN=CENTER><FONT SIZE=2>(<SPAN LANG="en-US">date of
			admission)</SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=958 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=2>УСЛОВИЯ ОПЛАТЫ <SPAN LANG="en-US">(payment
			terms)</SPAN></FONT></P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=534>
			<P ALIGN=CENTER><FONT SIZE=2><FONT FACE="Times New Roman, serif">%cpay0%</FONT>
			<FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">Оплата
			отправителем</SPAN></FONT> <FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">(</SPAN></FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">p</SPAN></FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">ayment
			by sender)</SPAN></FONT></FONT></P>
		</TD>
		<TD WIDTH=422>
			<P ALIGN=CENTER><FONT SIZE=2><FONT FACE="Times New Roman, serif">К
			оплате </FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">(for
			payment): %pay% руб.</SPAN></FONT></FONT></P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=534>
			<P ALIGN=CENTER><FONT SIZE=2><FONT FACE="Times New Roman, serif">%cpay1%</FONT>
			<FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">Оплата
			получателем</SPAN></FONT> <FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">(</SPAN></FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">p</SPAN></FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">ayment
			by </SPAN></FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="en-US">recipient</SPAN></FONT><FONT FACE="Times New Roman, serif"><SPAN LANG="ru-RU">)</SPAN></FONT></FONT></P>
		</TD>
		<TD WIDTH=422>
			<P ALIGN=CENTER><FONT SIZE=2>Вес, кг <SPAN LANG="en-US">(weight,
			kg): %mass% </SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=958 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=2>Причина недоставки
			корреспонденции <SPAN LANG="en-US">(the reason of
			non-delivery of correspondence)</SPAN></FONT></P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0cm; line-height: 0.2cm"><BR>
</P>
<TABLE WIDTH=962 BORDER=1 BORDERCOLOR="#000000" CELLPADDING=1 CELLSPACING=0>
	<COL WIDTH=295>
	<COL WIDTH=259>
	<COL WIDTH=61>
	<COL WIDTH=338>
	<TR>
		<TD COLSPAN=4 WIDTH=958 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=4>ПОЛУЧАТЕЛЬ (<SPAN LANG="en-US">the
			recipient): %recipient% </SPAN></FONT></P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=295>
			<P ALIGN=CENTER><FONT SIZE=3><SPAN LANG="ru-RU">Город(</SPAN><SPAN LANG="en-US">city,town): %rcity%</SPAN>
			</FONT>
			</P>
		</TD>
		<TD COLSPAN=2 WIDTH=322>
			<P ALIGN=CENTER><FONT SIZE=3><SPAN LANG="ru-RU">Страна</SPAN>
			<SPAN LANG="en-US">(country): %rcountry%</SPAN> </FONT>
			</P>
		</TD>
		<TD WIDTH=338>
			<P ALIGN=CENTER><FONT SIZE=3>Индекс <SPAN LANG="en-US">(zip
			code):%rzip%</SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=958 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=3><SPAN LANG="ru-RU">Адрес</SPAN>
			(<SPAN LANG="en-US">address): %raddress% </SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=958 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=2>ИНФОРМАЦИЯ О ВРУЧЕНИИ
			ОТПРАВЛЕНИЯ (information on the award of departure): %rdescription%</FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=958 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=2>Ф.И.О. (<SPAN LANG="en-US">name): %rf% %ri% %ro% </SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=4 WIDTH=958 VALIGN=TOP>
			<P ALIGN=CENTER><FONT SIZE=2>Должность <SPAN LANG="en-US">(regular
			appointment): %rappointment% </SPAN></FONT></P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD COLSPAN=2 WIDTH=556>
			<P ALIGN=CENTER><FONT SIZE=2>Дата (<SPAN LANG="en-US">date): %rdate% </SPAN></FONT></P>
		</TD>
		<TD COLSPAN=2 WIDTH=400>
			<P ALIGN=CENTER><FONT SIZE=2>Время (<SPAN LANG="en-US">time): %rtime% </SPAN></FONT></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=556 HEIGHT=93 VALIGN=TOP>
			<P STYLE="margin-bottom: 0.2cm"><FONT SIZE=1 STYLE="font-size: 8pt"><B>Получатель</B>
			подтверждает, что отправление принято
			в закрытом виде, отсутствуют внешние
			повреждения, претензий по качеству
			доставки нет.</FONT></P>
			<P><FONT SIZE=1 STYLE="font-size: 8pt">The recipient confirms that
			the </FONT><FONT SIZE=1 STYLE="font-size: 8pt"><SPAN LANG="en-US">package
			made in a</SPAN></FONT><FONT SIZE=1 STYLE="font-size: 8pt"> closed
			</FONT><FONT SIZE=1 STYLE="font-size: 8pt"><SPAN LANG="en-US">form</SPAN></FONT><FONT SIZE=1 STYLE="font-size: 8pt">,
			there </FONT><FONT SIZE=1 STYLE="font-size: 8pt"><SPAN LANG="en-US">is</SPAN></FONT><FONT SIZE=1 STYLE="font-size: 8pt">
			no external damage, </FONT><FONT SIZE=1 STYLE="font-size: 8pt"><SPAN LANG="en-US">N</SPAN></FONT><FONT SIZE=1 STYLE="font-size: 8pt">o
			complaints </FONT><FONT SIZE=1 STYLE="font-size: 8pt"><SPAN LANG="en-US">on</SPAN></FONT><FONT SIZE=1 STYLE="font-size: 8pt">
			quality delivery.</FONT></P>
		</TD>
		<TD COLSPAN=2 WIDTH=400>
			<P ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 10pt">Подпись
			(<SPAN LANG="en-US">signature)</SPAN></FONT></P>
		</TD>
	</TR>
</TABLE>
<TABLE WIDTH=962 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<COL WIDTH=962>
	<TR>
		<TD WIDTH=962 VALIGN=TOP>
			<P ALIGN=CENTER STYLE="margin-bottom: 0cm"><FONT SIZE=1 STYLE="font-size: 8pt">С
			подробными условиями доставки Вы
			можете ознакомиться в офисе курьерской
			службы.</FONT></P>
			<P LANG="en-US" ALIGN=CENTER><FONT SIZE=1 STYLE="font-size: 8pt">The
			detailed terms and conditions of delivery you can find in the
			office of the courier service</FONT></P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=CENTER STYLE="margin-bottom: 0cm"><BR>
</P>
</BODY>
</HTML>